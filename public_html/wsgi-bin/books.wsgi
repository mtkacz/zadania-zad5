#!/usr/bin/python
# -*- coding=utf-8 -*-

import sys, os
sys.path.append(os.path.dirname(__file__))

import bookdb
from urlparse import parse_qs
from cgi import escape

bodyIndex = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Library</title>
    </head>
    <body>
        Books list: <br />
        %s
    </body>
</html>
"""

bodyDetails = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Library</title>
    </head>
    <body>
        Books info: <br />
        %s<br />
        <a href="%s">Return to list</a>
    </body>
</html>
"""



def application(environ, start_response):
    content = ''
    booksInfo = bookdb.BookDB()

    parameters = parse_qs(environ.get('QUERY_STRING'))
    if 'id' in parameters:
        bookID = escape(parameters['id'][0])
        #details
        try:
            titles = booksInfo.title_info(bookID)
            content = """<p>
            Tittle: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%s <br />
            Author: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%s <br />
            Publisher: &nbsp;&nbsp;&nbsp;%s <br />
            ISBN: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%s
            </p>
            """ % (titles['title'], titles['author'], titles['publisher'], titles['isbn'])
        except:
            content = 'Wrong ID!<br />'
        response_body = bodyDetails % (
            content,
            environ.get('SCRIPT_NAME'),
        )

    else:
        #titles
        titles = booksInfo.titles()
        for entry in titles:
            content = content + "<a href='?id=" + entry['id'] + "'>" + entry['title'] + "</a><br />"

        response_body = bodyIndex % (
            content,
        )

    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 4464, application)
    srv.serve_forever()